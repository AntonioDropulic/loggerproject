#pragma once
///  \file Logger.h

/// \mainpage Logger
///  Logger is a pet project. Development is done as an excersicse in C++. It is used in small personal projects.
/// \section mainpage-header-1 How to use:
/// Logger's intended use is through the MACROS provided in the Logger.h file. 
/// Learn more about them in the MACROS documentation.
/// \section mainpage-header-2 What does the Logger do:
/// After being initialized to a global log level, and a single std::ostream of choice Logger
/// can be called to log to that ostream. See MACROS documentation for more details. 
/// Logger provides you with automatic linebreaks, time, and log level stamps.
/// \section problems  Current issues:
/// It might be wise to avoid macros. Macro LOG(level) shows a performance benefit over other known solutions.
/// To avoid the use of MACROS a dummy stream would have to be created and called on log priority check fail, so that following operator << calls aren't bad syntax. 
/// While this would be a less efficient it would improve on readability.
/// Repeating switch case in Logger::logLevelString and Logger::endlineStrategy and Logger::instance. 
/// How would i avoid it?
/// \section todo Future improvements:
/// These improvements are currently out of the scope of the project and will be researched and implemented at a later time.
/// \todo Multiple output streams coresponding to log levels for safety and better control over output.  
/// \todo Thread safety. Logging is currenty not thread safe.
/// \todo Tee stream that allows logging the same to multiple streams at once. Should provide additional safety and useful functionality.
/// \todo Signal handle flush. Flushing in the case of program crash.


#include <iostream>
#include <string>
#include <functional>

/// logLevels define logging priority - trace being lowest and off being highest - off enables us to turn off logging easily to save on performance.
/// a log cannot be made with a logLevel off - see LOG(level). 
enum class logLevel 
{
	trace = 0,
	debug,
	info,
	warn,
	error,
	off
};

/// \brief Handles stream input and linebreaking.
///
/// Intended use is as a temporary object. This is important because line breaking is done on object destruction, to be precise - parameter endlineStrategy is executed in the destructor. 
/// Input to the ostream is done using the operator <<. The operator forwards the input to the std::ostream, so all standard overloads are available.
class StreamDelegate
{
public:
	StreamDelegate(std::ostream& os, std::function<void(std::ostream&)> endlineStrategy /**<executed on object destruction, it should handle line breaking*/);
	
	~StreamDelegate();
	
	StreamDelegate(const StreamDelegate&) = delete;
	StreamDelegate& operator=(const StreamDelegate&) = delete;

	StreamDelegate(StreamDelegate&&) = default;
	StreamDelegate& operator=(StreamDelegate&&) = default;

	template<class T>
	inline StreamDelegate& operator<<(T&& output)
	{
		ostream << std::forward<T>(output);
		return *this;
	}

private:
	std::function<void(std::ostream&)> endline;
	std::ostream& ostream;
};

class Logger
{
public:
	///\brief instantiates a global Logger object
	///
	///For now it is not possible to change globalLogLevel and output stream dynamically. 
	/// \todo dynamic setting of Loggers members globalLogLevel and logStream
	static Logger& instance(logLevel globalLogLevel = logLevel::off, std::ostream& output = std::cout);

	~Logger();
	
	Logger(Logger const&) = delete;
	void operator=(Logger const&) = delete;
	///\brief this is it
	StreamDelegate log(logLevel level);
	
	logLevel getGlobalLogLevel();
protected:
	void timeStamp();
	void logLevelStamp(logLevel level);
	std::function<void(std::ostream&)> endlineStrategy(logLevel level);

	std::string logLevelString(logLevel level);
private:
	Logger();
	Logger(logLevel globalLogLevel, std::ostream& output);

	std::ostream& logStream;
	const logLevel globalLogLevel;
};


/// \brief Use to initialize the Logger.
///
/// Once set globalLogLevel and output can't be changed.
#define LOGGER_INIT(globalLogLevel, output) Logger::instance(globalLogLevel, output); 
/// \brief Call for a Log of a certain level. Should be used only after initialization.
///   
/// if the check passes a call logging is enabled and input is done with the operator <<.
/// If the check fails code Logger::instance.log() isn't called and the following operator << calls arent made.
#define LOG(level) if(Logger::instance().getGlobalLogLevel() <= (level) && (level) < logLevel::off) Logger::instance().log(level)
