#include "Logger.h"
#include <iomanip>



//PUBLIC LOGGER

Logger & Logger::instance(logLevel globalLogLevel, std::ostream& output) 
{
	static Logger instance(globalLogLevel, output);
	return instance;
}

Logger::~Logger()
{
}

StreamDelegate Logger::log(logLevel level)
{
	timeStamp();
	logLevelStamp(level);
	
	return StreamDelegate(this->logStream, this->endlineStrategy(level));
}

logLevel Logger::getGlobalLogLevel()
{
	return this->globalLogLevel;
}

//PROTECTED LOGGER
void Logger::logLevelStamp(logLevel level)
{
	// magic number - should I avoid it and how
	this->logStream << std::setw(7) << logLevelString(level) + ": ";
}

void Logger::timeStamp()
{
	this->logStream << __TIME__ << " ";
}

/// \todo avoid switch statement repetition
std::string Logger::logLevelString(logLevel level)
{
	//strategy?
	switch (level)
	{
		case logLevel::trace: return "TRACE";
		case logLevel::debug: return "DEBUG";
		case logLevel::info:  return "INFO";
		case logLevel::warn: return "WARN";
		case logLevel::error: return "ERROR";
		case logLevel::off: break;

		default: break;
	}
}
/// \todo avoid switch statement repetition
std::function<void(std::ostream&)> Logger::endlineStrategy(logLevel level)
{
	std::function<void(std::ostream&)> endlineNoFlush = [](std::ostream& os) {os << "\n"; };
	std::function<void(std::ostream&)> endlineFlush = [](std::ostream& os) {os << std::endl; };

	switch (level)
	{
	case logLevel::trace: return endlineNoFlush;
	case logLevel::debug: return endlineNoFlush;
	case logLevel::info:  return endlineNoFlush;
	case logLevel::warn: return endlineFlush;
	case logLevel::error: return endlineFlush;
	case logLevel::off: break;

	default: break;
	}
}

//PRIVATE LOGGER
Logger::Logger(logLevel globalLogLevel, std::ostream& output)
	: globalLogLevel(globalLogLevel), logStream(output)
{
}

//PUBLIC STREAM DELEGATE
StreamDelegate::StreamDelegate(std::ostream & os, std::function<void(std::ostream&)> endline)
	:
	ostream(os), endline(endline)
{
}

StreamDelegate::~StreamDelegate()
{
	endline(ostream);
}


