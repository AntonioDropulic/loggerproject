#include "Logger.h"

int main()
{
	LOGGER_INIT(logLevel::debug, std::cout);

	LOG(logLevel::trace) << "Dummy line. " << "No input.";
	LOG(logLevel::info) << "First line.";
	LOG(logLevel::warn) << "Second: first input. " << "Second line: second input.";

	std::cin.get();
}


