var _logger_8h =
[
    [ "StreamDelegate", "class_stream_delegate.html", "class_stream_delegate" ],
    [ "Logger", "class_logger.html", "class_logger" ],
    [ "LOG", "_logger_8h.html#aba7b09d6e8fbe414c23705ad24dde6ff", null ],
    [ "LOGGER_INIT", "_logger_8h.html#af4115a541d105430e69f168375397209", null ],
    [ "logLevel", "_logger_8h.html#a474fc5e1d06d669823896ac6258fbc0d", [
      [ "trace", "_logger_8h.html#a474fc5e1d06d669823896ac6258fbc0da04a75036e9d520bb983c5ed03b8d0182", null ],
      [ "debug", "_logger_8h.html#a474fc5e1d06d669823896ac6258fbc0daad42f6697b035b7580e4fef93be20b4d", null ],
      [ "info", "_logger_8h.html#a474fc5e1d06d669823896ac6258fbc0dacaf9b6b99962bf5c2264824231d7a40c", null ],
      [ "warn", "_logger_8h.html#a474fc5e1d06d669823896ac6258fbc0da1ea4c3ab05ee0c6d4de30740443769cb", null ],
      [ "error", "_logger_8h.html#a474fc5e1d06d669823896ac6258fbc0dacb5e100e5a9a3e7f6d1fd97512215282", null ],
      [ "off", "_logger_8h.html#a474fc5e1d06d669823896ac6258fbc0da3262d48df5d75e3452f0f16b313b7808", null ]
    ] ]
];