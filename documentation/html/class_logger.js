var class_logger =
[
    [ "~Logger", "class_logger.html#acb668a9e186a25fbaad2e4af6d1ed00a", null ],
    [ "Logger", "class_logger.html#a0d21ab21689125fbb5e91584e4f37baa", null ],
    [ "endlineStrategy", "class_logger.html#a31cd484776d7e605f4bbcd3bffd623a5", null ],
    [ "getGlobalLogLevel", "class_logger.html#a9d84d218cc159303ce4c353939caaf58", null ],
    [ "log", "class_logger.html#a17cefefd6dcf9d553ffd72f3b18434d7", null ],
    [ "logLevelStamp", "class_logger.html#addff553e9c1d3c52667a51b28f999ad3", null ],
    [ "logLevelString", "class_logger.html#a0598bd82e2421d86486556cea4e2b3dc", null ],
    [ "operator=", "class_logger.html#ae14ecca1071f841760f40b5a1398580c", null ],
    [ "timeStamp", "class_logger.html#a74fb8ba6e81751553da10562de8884a1", null ]
];